<?php

/**
 * @file
 * Profile type editing UI.
 */

/**
 * UI controller.
 */
class Profile2TypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage profiles, including fields.';
    return $items;
  }
}

/**
 * Generates the profile type editing form.
 */
function profile2_type_form($form, &$form_state, $profile_type, $op = 'edit') {

  // Build the list of entities that are allowed to have profile2 attached.
  if (!($entities = module_invoke_all('profile2_info'))) {
    $form['message'] = array(
      '#type' => 'markup',
      '#markup' => t('There are no modules providing entities for attaching Profile2 profiles. You must enable first at least one module like <em>Profile2 User</em> to be able to create profiles. Developers may define such entities by implementing <code>hook_profile2_info()</code>.'),
    );
    return $form;
  }

  if ($op == 'clone') {
    $profile_type->label .= ' (cloned)';
    $profile_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $profile_type->label,
    '#description' => t('The human-readable name of this profile type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($profile_type->type) ? $profile_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $profile_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'profile2_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this profile type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['entity'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Entities'),
    '#description' => t('Entities where this Profile2 profile type is attached.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['data'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#description' => t('Various settings for each entity wher this Profile2 profile type is attached.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $entity_info =& entity_get_info();
  foreach ($entities as $entity_type => $entity_data) {
    $entity_label = $entity_info[$entity_type]['label'];
    $title = format_string('@label [@name]', array('@label' => $entity_label, '@name' => $entity_type));
    $description = $entity_info[$entity_type]['description'];

    $form['entity'][$entity_type] = array(
      '#type' => 'checkbox',
      '#title' => $title,
      '#description' => $description,
      '#default_value' => (isset($profile_type->entity)  && in_array($entity_type, $profile_type->entity)) ? $entity_type : FALSE,
      '#return_value' => $entity_type,
    );

    // See if the module has implemented settings and expose them in the form.
    if (isset($entity_data['settings form']) && function_exists($function = $entity_data['settings form']) && is_array($module_form = $function($form_state))) {
      $form['data'][$entity_type] = array(
        '#type' => 'fieldset',
        '#title' => $entity_label,
        '#states' => array(
          'visible' => array(
            ':input[name="entity[' . $entity_type . ']"]' => array('checked' => TRUE),
          ),
        ),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['data'][$entity_type] += $module_form;
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save profile type'),
    '#weight' => 40,
  );

  if (!$profile_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete profile type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('profile2_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Form API validation callback for the type form.
 */
function profile2_type_form_validate(&$form, &$form_state) {
  if (!array_filter($form_state['values']['entity'])) {
    form_set_error('entity', t('You must select at least one entity where this profile type will be attached.'));
  }
}

/**
 * Form API submit callback for the type form.
 */
function profile2_type_form_submit(&$form, &$form_state) {

  // Make entity array clean.
  $form_state['values']['entity'] = array_values(array_filter($form_state['values']['entity']));

  $profile_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $profile_type->save();
  $form_state['redirect'] = 'admin/structure/profiles';
}

/**
 * Form API submit callback for the delete button.
 */
function profile2_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/profiles/manage/' . $form_state['profile2_type']->type . '/delete';
}
