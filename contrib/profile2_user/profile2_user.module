<?php

/**
 * @file
 * Adds support for user entity.
 */

/**
 * Implements hook_profile2_info().
 *
 * This is the first thing a module should implement in order to attach profiles
 * to an arbitrary entity.
 */
function profile2_user_profile2_info() {
  return array(
    'user' => array(
      'uri callback' => 'profile2_user_uri_callback',
      'settings form' => 'profile2_user_settings_form',
    ),
  );
}

/**
 * Implements hook_profile2_uri_callback().
 *
 * @see profile2_uri_callback()
 */
function profile2_user_uri_callback($profile) {
  return array(
    'path' => 'user' . (isset($profile->entity_id) ? '/' . $profile->entity_id : ''),
    'options' => array('fragment' => 'profile-' . $profile->type),
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function profile2_user_entity_info_alter(&$entity_info) {
  // Add new view modes for account.
  $entity_info['profile2']['view modes']['account'] = array(
    'label' => t('User account'),
    'custom settings' => FALSE,
  );
}

/**
 * Fetch profiles by account.
 *
 * @param $account
 *   The user account to load profiles for, or its uid.
 * @param $type_name
 *   To load a single profile, pass the type name of the profile to load.
 * @return
 *   Either a single profile or an array of profiles keyed by profile type.
 *
 * @see profile2_load_multiple()
 * @see profile2_profile2_delete()
 * @see Profile::save()
 */
function profile2_user_load_by_user($account, $type_name = NULL) {
  // Use a separate query to determine all profile ids per user and cache them.
  // That way we can look up profiles by id and benefit from the static cache
  // of the entity loader.
  $cache = &drupal_static(__FUNCTION__, array());
  $uid = is_object($account) ? $account->uid : $account;

  if (!isset($cache[$uid])) {
    if (empty($type_name)) {
      $profiles = profile2_load_multiple(FALSE, array('entity_type' => 'user', 'entity_id' => $uid));
      // Cache ids for further lookups.
      $cache[$uid] = array();
      foreach ($profiles as $pid => $profile) {
        $cache[$uid][$profile->type] = $pid;
      }
      return $profiles ? array_combine(array_keys($cache[$uid]), $profiles) : array();
    }
    $cache[$uid] = db_select('profile', 'p')
      ->fields('p', array('type', 'pid'))
      ->condition('p.entity_type', 'user')
      ->condition('p.entity_id', $uid)
      ->execute()
      ->fetchAllKeyed();
  }
  if (isset($type_name)) {
    return isset($cache[$uid][$type_name]) ? profile2_load($cache[$uid][$type_name]) : FALSE;
  }
  // Return an array containing profiles keyed by profile type.
  return $cache[$uid] ? array_combine(array_keys($cache[$uid]), profile2_load_multiple($cache[$uid])) : $cache[$uid];
}

/**
 * Implements hook_profile2_update().
 */
function profile2_user_profile2_update(Profile $profile) {
  if ($profile->entity_type == 'user') {
    // Update the static cache from profile2_user_load_by_user().
    $cache = &drupal_static('profile2_user_load_by_user', array());
    if (isset($cache[$profile->entity_id])) {
      $cache[$profile->entity_id][$profile->type] = $profile->pid;
    }
  }
}

/**
 * Implements hook_profile2_delete().
 */
function profile2_user_profile2_delete($profile) {
  // Clear the static cache from profile2_user_load_by_user().
  $cache = &drupal_static('profile2_user_load_by_user', array());
  unset($cache[$profile->entity_id][$profile->type]);
}

/**
 * Implements hook_user_delete().
 */
function profile2_user_user_delete($account) {
  foreach (profile2_user_load_by_user($account) as $profile) {
    profile2_delete($profile);
  }
}

/**
 * Implements hook_user_categories().
 */
function profile2_user_user_categories() {
  $data = array();
  foreach (profile2_get_types() as $type => $profile_type) {
    if (in_array('user', $profile_type->entity) && $profile_type->data['user']['category']) {
      $data[] = array(
        'name' => $type,
        'title' => $profile_type->label,
        // Add an offset so a weight of 0 appears right of the account category.
        'weight' => $profile_type->weight + 3,
        'access callback' => 'profile2_user_category_access',
        'access arguments' => array(1, $profile_type)
      );
    }
  }
  return $data;
}

/**
 * Implements hook_user_view().
 */
function profile2_user_user_view($account, $view_mode, $langcode) {
  foreach (profile2_get_types() as $type => $profile_type) {
    if ($profile_type->data['user']['view'] && ($profile = profile2_user_load_by_user($account, $type))) {
      if (profile2_access('view', $profile)) {
        $account->content['profile_' . $type] = array(
          '#type' => 'user_profile_category',
          '#title' => $profile->label,
          '#prefix' => '<a id="profile-' . $profile->type . '"></a>',
        );
        $account->content['profile_' . $type]['view'] = $profile->view('page');
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for the user edit form.
 *
 * @see profile2_form_validate_handler
 * @see profile2_form_submit_handler
 */
function profile2_user_form_user_profile_form_alter(&$form, &$form_state) {
  if (($type = profile2_get_types($form['#user_category'])) && in_array('user', $type->entity) && $type->data['user']['category']) {
    if (empty($form_state['profiles'])) {
      $profile = profile2_user_load_by_user($form['#user'], $form['#user_category']);
      if (empty($profile)) {
        $profile = profile2_create(array('type' => $form['#user_category'], 'entity_type' => 'user', 'entity_id' => $form['#user']->uid));
      }
      $form_state['profiles'][$profile->type] = $profile;
    }
    profile2_attach_form($form, $form_state);
  }
}

/**
 * Form settings callback for profile type form.
 */
function profile2_user_settings_form(&$form_state) {
  $form['registration'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show during user account registration'),
    '#description' => t('If checked, this profile type will be displayed in a fieldset inside the user account registration form.'),
    '#default_value' => !empty($form_state['profile2_type']->data['user']['registration']),
  );
  $form['category'] = array(
    '#type' => 'checkbox',
    '#title' => t('Profile editable in user categories'),
    '#description' => t('If checked, this profile can be edited in user categories, under <code>user/[UID]/edit/[PROFILE_TYPE]</code>.'),
    '#default_value' => !empty($form_state['profile2_type']->data['user']['category']),
  );
  $form['view'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display on user account page'),
    '#description' => t('If checked, this profile type will be dispalyed on the user account page.'),
    '#default_value' => !empty($form_state['profile2_type']->data['user']['view']),
  );
  return $form;
}

/**
 * Attempt to load a profile type by user ID.
 *
 * Returns the profile object for the given user. If there is none yet, a new
 * object is created.
 *
 * @param $uid
 *   Integer with the user ID.
 * @param $type_name
 *   String with the type name.
 *
 * @return
 *   A Profile object.
 */
function profile2_user_by_uid_load($uid, $type_name) {
  if ($uid && is_numeric($uid) && ($account = user_load($uid))) {
    $profile = profile2_user_load_by_user($account, $type_name);
    if (!$profile) {
      $profile = profile2_create(array('type' => $type_name, 'entity_type' => 'user', 'entity_id' => $uid));
    }
    return $profile;
  }
  return FALSE;
}

/**
 * Menu item access callback - check if a user has access to a profile category.
 */
function profile2_user_category_access($account, $profile_type) {
  // As there might be no profile yet, create a new object for being able to run
  // a proper access check.
  $profile = profile2_create(array('type' => $profile_type->type, 'entity_type' => 'user', 'entity_id' => $account->uid));
  return (($account->uid > 0) && $profile_type->data['user']['category'] && profile2_access('edit', $profile, $account));
}

/**
 * Implements hook_profile2_access().
 *
 * @see hook_profile2_access()
 * @see profile2_access()
 */
function profile2_user_profile2_access($op, $profile = NULL, $account = NULL) {

  // Don't grant access for users to delete their profile.
  if (isset($profile) && ($profile->entity_type == 'user') && ($type_name = $profile->type) && $op != 'delete') {
    if (user_access("$op any $type_name profile", $account)) {
      return TRUE;
    }
    $account = isset($account) ? $account : $GLOBALS['user'];
    if (isset($profile->entity_id) && $profile->entity_id == $account->uid && user_access("$op own $type_name profile", $account)) {
      return TRUE;
    }
  }
  // Do not explicitly deny access so others may still grant access.
}

/**
 * Implements hook_form_FORM_ID_alter() for the registration form.
 */
function profile2_user_form_user_register_form_alter(&$form, &$form_state) {
  foreach (profile2_get_types() as $type_name => $profile_type) {
    if (!empty($profile_type->data['user']['registration']) && in_array('user', $profile_type->entity)) {
      if (empty($form_state['profiles'][$type_name])) {
        $form_state['profiles'][$type_name] = profile2_create(array('type' => $type_name, 'entity_type' => 'user'));
      }
      profile2_attach_form($form, $form_state);
      // Wrap each profile form in a fieldset.
      $form['profile_' . $type_name] += array(
        '#type' => 'fieldset',
        '#title' => check_plain($profile_type->label),
      );
    }
  }
}

/**
 * Implements hook_profile2_submit_profile_alter().
 *
 * During registration phase set the uid field of the newly created user to the
 * profile Profile object.
 *
 * @see hook_profile2_submitted_profile_alter()
 * @see profile2_form_submit_handler()
 * @see drupal_alter()
 */
function profile2_user_profile2_submitted_profile_alter(&$profile, $form_state) {
  if (empty($profile->entity_id) && $profile->entity_type == 'user' && isset($form_state['user']->uid)) {
    $profile->entity_id = $form_state['user']->uid;
  }
}

/**
 * Submit handler that builds and saves all profiles in the form.
 *
 * @see profile2_attach_form()
 */
function profile2_user_form_submit_handler(&$form, &$form_state) {
  foreach ($form_state['profiles'] as $type => $profile) {
    // During registration set the uid field of the newly created user.
    if (empty($profile->entity_id) && isset($form_state['user']->uid)) {
      $form_state['profiles'][$type]->entity_id = $form_state['user']->uid;
    }
  }
}

/**
 * Entity metadata callback to load profiles for the given user account.
 */
function profile2_user_get_properties($account, array $options, $name) {
  // Remove the leading 'profile_' from the property name to get the type name.
  $profile = profile2_user_load_by_user($account, substr($name, 8));
  return $profile ? $profile : NULL;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds a checkbox for controlling field view access to fields added to
 * profiles.
 */
function profile2_user_form_field_ui_field_edit_form_alter(&$form, &$form_state) {

  // Load the Profile2 profile type.
  $profile_type = profile2_get_types($form['instance']['bundle']['#value']);
  //  Check if this type is attached to user entity.
  if (!in_array('user', $profile_type->entity)) {
    return;
  }

  if ($form['instance']['entity_type']['#value'] == 'profile2') {
    $form['field']['settings']['profile2_private'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make the content of this field private.'),
      '#default_value' => !empty($form['#field']['settings']['profile2_private']),
      '#description' => t('This setting is used only in User profiles. If checked, the content of this field is only shown to the profile owner and administrators.'),
    );
  }
  else {
    // Add the value to the form so it isn't lost.
    $form['field']['settings']['profile2_private'] = array(
      '#type' => 'value',
      '#value' => !empty($form['#field']['settings']['profile2_private']),
    );
  }
}

/**
 * Implements hook_field_access().
 */
function profile2_user_field_access($op, $field, $entity_type, $profile = NULL, $account = NULL) {
  if (!user_access('administer profiles', $account) && $entity_type == 'profile2' && $profile->entity_type == 'user' && $op == 'view' && !empty($field['settings']['profile2_private'])) {
    // For profiles, deny general view access for private fields.
    if (!isset($profile)) {
      return FALSE;
    }
    // Also deny view access, if someone else views a private field.
    $account = isset($account) ? $account : $GLOBALS['user'];
    if ($account->uid != $profile->entity_id) {
      return FALSE;
    }
  }
}
